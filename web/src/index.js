import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

import AccountPage from './components/account';
import WeatherPage from './components/weather';

const TAG = 'WEATHER_APP_USERNAME';

function App() {
    const [logged, setLoginStatus] = useState(false);

    useEffect(() => {
        let username = sessionStorage.getItem(TAG);
        if (username) {
            setLoginStatus(true);
        }
    });

    function onLogin(username) {
        setLoginStatus(true);
        sessionStorage.setItem(TAG, username);
    }
    return (
        <div className="layout">
            {logged
                ? <WeatherPage onLogout={() => setLoginStatus(false)} />
                : <AccountPage onLogin={onLogin} />}
        </div>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
