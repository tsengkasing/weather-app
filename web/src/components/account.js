import React, { Component } from 'react';
import { Input, Button, Tabs } from 'antd';

import '../stylus/account.styl';

import { login, register } from '../util/api';

const TabPane = Tabs.TabPane;

class AccountPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 'login',

            username: '',
            password: '',
        };
    }

    handleSwitchTab = (key) => {
        this.setState({
            tab: key,
            password: ''
        });
    }

    handleInputUsername = (event) => this.setState({ username: event.target.value })
    handleInputPassword = (event) => this.setState({ password: event.target.value })

    handleSubmitLogin = () => {
        const { username, password } = this.state;
        if (!username || !password) {
            return alert('Please fill in all blanks');
        }
        login(username, password)
            .then(() => {
                alert('Login Success!');
                typeof this.props.onLogin === 'function' && this.props.onLogin();
            }).catch(err => {
                alert(err);
            });
    }

    handleSubmitRegister = () => {
        const { username, password } = this.state;
        if (!username || !password) {
            return alert('Please fill in all blanks');
        }
        register(username, password)
            .then(() => {
                alert('Register Success! Please login.');
            }).catch(err => {
                alert(err);
            });
    }

    render() {
        const {tab, username, password} = this.state;
        return (
            <section className="account__layout">
                <Tabs activeKey={tab} onChange={this.handleSwitchTab}>
                    <TabPane tab="Login" key="login">
                        <Input className="field" placeholder="Email" value={username}
                            type="email" onChange={this.handleInputUsername} />
                        <Input.Password className="field" placeholder="Password"
                            value={password} onChange={this.handleInputPassword} />
                        <Button className="field" type="primary" block
                            onClick={this.handleSubmitLogin}>Login</Button>
                    </TabPane>
                    <TabPane tab="Register" key="register">
                        <Input className="field" placeholder="Email" value={username}
                            type="email" onChange={this.handleInputUsername} />
                        <Input.Password className="field" placeholder="Password"
                            value={password} onChange={this.handleInputPassword} />
                        <Button className="field" type="primary" block
                            onClick={this.handleSubmitRegister}>Register</Button>
                    </TabPane>
                </Tabs>
            </section>
        );
    }
}

export default AccountPage;
