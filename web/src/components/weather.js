import React, { Component } from 'react';
import { Select, DatePicker, Button, Table, Divider, Tag } from 'antd';

import { queryWeather } from '../util/api';

const { RangePicker } = DatePicker;
const Option = Select.Option;

import '../stylus/weather.styl';

const columns = [{
    title: 'Time',
    dataIndex: 'dt',
    key: 'dt'
},{
    title: 'Main',
    dataIndex: 'weather_main',
    key: 'weather_main',
}, {
    title: 'Description',
    dataIndex: 'weather_description',
    key: 'weather_description',
}, {
    title: 'Temperature',
    dataIndex: 'temp',
    key: 'temp',
}, {
    title: 'Pressure',
    dataIndex: 'pressure',
    key: 'pressure',
}, {
    title: 'Humidity',
    dataIndex: 'humidity',
    key: 'humidity',
}, {
    title: 'Min Temperature',
    dataIndex: 'temp_min',
    key: 'temp_min',
}, {
    title: 'MaxTemperature',
    dataIndex: 'temp_max',
    key: 'temp_max',
}, {
    title: 'Visibility',
    dataIndex: 'visibility',
    key: 'visibility',
}, {
    title: 'Wind speed',
    dataIndex: 'wind_speed',
    key: 'wind_speed',
}, {
    title: 'Wind degree',
    dataIndex: 'wind_deg',
    key: 'wind_deg',
}];

class WeatherPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            city: 'hong kong',
            fromTime: '',
            toTime: '',
            data: []
        };
    }

    handleSwitchCity = (value) => this.setState({ city: value })
    handleChangeTimeRange = (value, dateString) => {
        const [fromTime, toTime] = dateString;
        this.setState({ fromTime, toTime });
    }

    // Search Weather Data
    handleSearch = () => {
        let { city, fromTime, toTime } = this.state;
        if (fromTime) {
            fromTime = (new Date(fromTime)).getTime() / 1000; // Unix time
        }
        if (toTime) {
            toTime = (new Date(toTime)).getTime() / 1000 // Unix time
        }
        queryWeather(city, fromTime, toTime).then(data => {
            if (!data) {
                alert('Access Denied');
                window.sessionStorage.clear();
                window.location.reload();
            } else this.setState({
                data: data.map(({id, dt, ...props}) => ({
                    ...props,
                    key: dt,
                    dt: (new Date(dt * 1000)).toLocaleString()
                }))
            });
        }).catch(err => {
            alert(err);
        });
    }

    render() {
        const { city, data } = this.state;
        return (
            <section className="weather__layout">
                <div className="input__fields">
                    <Select className="field" value={city} style={{ width: 120 }}
                        onChange={this.handleSwitchCity}>
                        <Option value="hong kong">Hong Kong</Option>
                        <Option value="singapore">Singapore</Option>
                    </Select>
                    <RangePicker
                        className="field"
                        showTime={{ format: 'HH:mm' }}
                        format="YYYY-MM-DD HH:mm"
                        placeholder={['Start Time', 'End Time']}
                        onChange={this.handleChangeTimeRange}
                        />
                    <Button className="field" type="primary"
                        onClick={this.handleSearch}>Search</Button>
                </div>
                <div className="info">
                    <Table columns={columns} dataSource={data} />
                </div>
            </section>
        );
    }
}

export default WeatherPage;
