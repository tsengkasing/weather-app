/**
 * @fileoverview
 * API list
 */

/**
 * Registrer API
 * @param {string} username
 * @param {string} password
 */
export function register(username, password) {
    return new Promise((resolve, reject) => {
        fetch(`/api/account/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'cache-control': 'no-store',
            },
            body: `mail=${username}&password=${password}`
        }).then(
            response => response.json()
        ).then(({status, msg}) => {
            if (status === 0) {
                resolve();
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

/**
 * Login API
 * @param {string} username
 * @param {string} password
 */
export function login(username, password) {
    return new Promise((resolve, reject) => {
        fetch(`/api/account/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'cache-control': 'no-store',
            },
            body: `mail=${username}&password=${password}`
        }).then(
            response => response.json()
        ).then(({status, msg}) => {
            if (status === 0) {
                resolve();
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}

/**
 * Weather API
 * @param {string} username
 * @param {string} password
 */
export function queryWeather(city, start, end) {
    return new Promise((resolve, reject) => {
        fetch(`/api/weather/${city}?start=${start}&end=${end}`, {
            method: 'GET'
        }).then(response => {
            if (response.status === 403) {
                resolve(null);
            } else return response.json();
        }).then(({status, msg, data}) => {
            if (status === 0) {
                resolve(data);
            } else {
                reject(msg);
            }
        }).catch(err => reject(err));
    });
}
