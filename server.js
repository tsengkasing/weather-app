/**
 * @fileoverview
 * server entry
 */

const path = require('path');
const { logInfo, logError } = require('./service/logger');
const WeatherFetcher = require('./service/weather_fetcher');

/**
 * Load configuration
 * @type {{database: {host: string, port: number, username: string, password: string, schema: string}, server_port: number}}
 */
let config;
try {
    config = require('./config');
} catch (e) {
    console.error(e.message + ' Configuration File Not Found! Please copy config.template.js to config.js & write your configuration.');
    process.exit(1);
}

const {database: {host, port, username, password, schema}, server_port: SERVER_PORT, APPID} = config;

const DB = require('./service/db');
const fastify = require('fastify')({
    // logger: true
});

// Content type parser for 'application/x-www-form-urlencoded'
fastify.register(require('fastify-formbody'));

// Cookie Plugin
fastify.register(require('fastify-cookie'));

// Connect mysql
fastify.register(require('fastify-mysql'), {
    promise: true,
    connectionString: `mysql://${username}:${password}@${host}:${port}/${schema}`
});

// Serve static files
fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public'),
    prefix: '/'
});

// Load Controllers
fastify.register(require('./controller/account'), { prefix: '/api/account' });
fastify.register(require('./controller/weather'), { prefix: '/api/weather' });

// Response Content Type
fastify.addHook('onSend', (request, reply, payload, next) => {
    if (/\/api/.test(request.raw.url)) {
        reply.type('application/json; charset=utf8');
    }
    next();
});

// Start Server
fastify.listen(SERVER_PORT, '0.0.0.0', function (err, address) {
    if (err) {
        fastify.log.error(err);
        process.exit(1);
    }
    fastify.mysql.getConnection().then(conn => {
        const db = new DB(conn);
        fastify.decorate('db', db);
        scheduler(db);
        logInfo(`server listening on ${address}`);
    }).catch(err => {
        logError(err);
        process.exit(1);
    });
});

const weatherFetcher = new WeatherFetcher(APPID);
/**
 * Schedule to fetch weather from API
 * @param {DB} db Database connection
 * @param {Array<string>} cityList
 * @param {number} timeInterval scheduler interval
 */
async function scheduler(db, cityList = ['hong kong', 'singapore'], timeInterval = 60000) {
    if (!Array.isArray(cityList)) throw new Error('Error format of city list');
    for (const city of cityList) {
        try {
            const {cod, ...data} = await weatherFetcher.fetchWeatherData(city);
            if (cod !== 200) continue;
            const {
                weather: [{main: weather_main, description: weather_description}],
                main: {temp, pressure, humidity, temp_max, temp_min},
                visibility,
                wind: {speed: wind_speed, deg: wind_deg},
                clouds: {all: clouds},
                rain,
                snow,
                dt,
                sys: {sunrise, sunset}
            } = data;

            let rain_1h = null;
            let rain_3h = null;
            let snow_1h = null;
            let snow_3h = null;
            if (rain) { ({'1h': rain_1h, '3h': rain_3h} = rain); }
            if (snow) { ({'1h': snow_1h, '3h': snow_3h} = rain); }

            await db.executeSql(`
            INSERT INTO weather (
                city, weather_main, weather_description, temp, pressure, humidity, temp_min, temp_max, rain_1h, rain_3h,
                snow_1h, snow_3h, visibility, wind_speed, wind_deg, clouds, dt, sunrise, sunset
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            `, [
                city, weather_main, weather_description, temp, pressure, humidity, temp_min, temp_max, rain_1h, rain_3h,
                snow_1h, snow_3h, visibility, wind_speed, wind_deg, clouds, dt, sunrise, sunset
            ]);
            logInfo(`[${city}] weather fetched`);
        } catch (err) {
            logError(err);
            continue;
        }
    }
    setTimeout(scheduler, timeInterval, db, cityList, timeInterval);
}
