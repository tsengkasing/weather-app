/**
 * @fileoverview
 * encrypt password
 */

const bcrypt = require('bcrypt');
const crypto = require('crypto');

/**
 * @param {string} password
 * @returns {Promise<string>}
 */
exports.cryptPassword = function(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) return reject(err);
            bcrypt.hash(password, salt, function(err, hash) {
                if (err) reject(err);
                else resolve(hash);
            });
        });
    });
};

/**
 * @param {string} plainPass
 * @param {string} hashword
 * @returns {boolean}
 */
exports.comparePassword = function(plainPass, hashword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(plainPass, hashword, function(err, isPasswordMatch) {
            if (err) reject(err);
            else resolve(isPasswordMatch);
        });
    });
};

const password = crypto.randomBytes(16);
const algorithm = 'aes-128-cbc';
const iv = crypto.randomBytes(16);

/**
 * @param {string} text
 */
exports.encrypt = function(text) {
    const cipher = crypto.createCipheriv(algorithm, Buffer.from(password), iv);
    cipher.setAutoPadding(true);
    let crypted = cipher.update(text, 'utf8', 'binary');
    crypted += cipher.final('binary');
    return crypted.toString();
};

/**
 * @param {string} text
 */
exports.decrypt = function(text) {
    const decipher = crypto.createDecipheriv(algorithm, Buffer.from(password), iv);
    let dec = decipher.update(text, 'binary', 'utf8');
    dec += decipher.final('utf8');
    return dec.toString();
};
