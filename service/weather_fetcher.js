/**
 * @fileoverview
 * Fetch weather information
 */

const http = require('http');

class WeatherFetcher {
    /**
     * construcotr
     * @param {string} appid
     */
    constructor(appid) {
        if (!appid) {
            throw new Error('Missing APPID of openweathermap');
        }
        this._appid = appid;
    }

    /**
     * Fetch Weather information from API
     * @param {string} location
     * @returns {{cod: number, weather: {main: string, description: string}, main: {temp: number, pressure: number, humidity: number, temp_min: number, temp_max: number}, visibility: number, wind: {speed: number, deg: number}, clouds: {all: number}, rain: {1h: number, 3h: number} | null, snow: {1h: number, 3h: number} | null, dt: number, sys: {sunrise: number, sunset: number}}}
     */
    fetchWeatherData(location = 'hong kong') {
        return new Promise((resolve, reject) => {
            const req = http.request({
                host: 'api.openweathermap.org',
                path: encodeURI(`/data/2.5/weather?q=${location}&appid=${this._appid}`),
                method: 'GET'
            }, (response) => {
                const chunks = [];
                response.on('data', chunk => chunks.push(chunk));
                response.on('end', () => {
                    let body = Buffer.concat(chunks).toString();
                    try {
                        body = JSON.parse(body);
                    } catch (err) {
                        return reject(err);
                    }
                    resolve(body);
                });
            });
            req.end();
        });
    }
}

module.exports = WeatherFetcher;
