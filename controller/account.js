/**
 * @fileoverview
 * account controller
 */

const { logError } = require('../service/logger');
const { responseJSON } = require('../service/response');
const { cryptPassword, comparePassword, encrypt } = require('../service/encrypt');

module.exports = function (fastify, options, next) {
    /**
     * User Registry
     */
    fastify.post('/register', async (req, reply) => {
        const { mail, password } = req.body;
        try {
            // check whether user email exist
            let result = await fastify.db.fetchOne(`
                SELECT mail FROM account WHERE mail = ?;
            `, [mail]);
            if (result) {
                return responseJSON(1, 'Email existed!');
            }

            const encryptedPassword = await cryptPassword(password);

            // create user
            result = await fastify.db.executeSql(`
                INSERT INTO account (mail, password) VALUES (?, ?);
            `, [mail, encryptedPassword]);
            return responseJSON(0, 'OK');
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });

    /**
     * User Login
     */
    fastify.post('/login', async (req, reply) => {
        const { mail, password } = req.body;
        try {
            // check whether user email exist
            let result = await fastify.db.fetchOne(`
                SELECT mail, password FROM account WHERE mail = ?;
            `, [mail]);
            if (!result) {
                return responseJSON(1, 'User not existed!');
            }

            // compare password
            const hashedPassword = result['password'];
            let isMatched = await comparePassword(password, hashedPassword);

            // password matched
            if (isMatched) {
                reply.setCookie('_session', encrypt(mail), {
                    maxAge: 259200, // 3 days
                    path: '/',
                    httpOnly: true
                });
                return responseJSON(0, 'OK');
            }
            return responseJSON(1, 'Password Not Matched!');
        } catch (err) {
            logError(err.message);
            return responseJSON(1, err.message);
        }
    });

    next();
};
