/**
 * @fileoverview
 * weather controller
 */

const { responseJSON } = require('../service/response');
const { decrypt } = require('../service/encrypt');

module.exports = function (fastify, options, next) {
    /**
     * Query Weather
     */
    fastify.get('/:city', async (req, reply) => {
        const cookie = req.cookies['_session'];
        if (!cookie) {
            reply.code(403);
            return responseJSON(1, 'Please Login');
        }

        const username = decrypt(cookie);
        let result = await fastify.db.fetchOne(`
            SELECT mail FROM account WHERE mail = ?;
        `, [username]);
        // User not exist
        if (!result) {
            reply.setCookie('_session', '', {
                maxAge: 0, // expire now
                path: '/',
                httpOnly: true
            });
            reply.code(403);
            return responseJSON(1, 'Access Forbidden');
        }

        const city = req.params['city'];
        if (!city) {
            return responseJSON(1, 'Missing Parameters');
        }

        let { start, end } = req.query;
        if ((start && !end) || (!start && end)) {
            return responseJSON(1, 'Time Range must should have beginning and ending.');
        } else if (!start && !end) {
            // if not given start & end time
            // use data from yesterday to today
            const d = new Date();
            end = d.getTime() / 1000;
            start = d.setDate(d.getDate() - 1) / 1000;
        }

        try {
            let result = await fastify.db.fetchAll(`
                SELECT
                    dt, id,
                    weather_main, weather_description,
                    temp, pressure, humidity, temp_min, temp_max,
                    visibility, wind_speed, wind_deg
                FROM weather
                WHERE city = ?
                    AND dt BETWEEN ? AND ?;
            `, [city, start, end]);
            return responseJSON(0, 'OK', result);
        } catch (err) {
            return responseJSON(1, err.message);
        }
    });

    next();
};
