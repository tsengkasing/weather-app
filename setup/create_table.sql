DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `mail` VARCHAR(32) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  KEY (`mail`)
) CHARACTER SET utf8mb4;

DROP TABLE IF EXISTS `weather`;
CREATE TABLE `weather` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `city` VARCHAR(32) NOT NULL,
  `weather_main` VARCHAR(64) NULL,
  `weather_description` VARCHAR(64) NULL,
  `temp` DOUBLE NULL,
  `pressure` DOUBLE NULL,
  `humidity` DOUBLE NULL,
  `temp_min` DOUBLE NULL,
  `temp_max` DOUBLE NULL,
  `rain_1h` DOUBLE NULL,
  `rain_3h` DOUBLE NULL,
  `snow_1h` DOUBLE NULL,
  `snow_3h` DOUBLE NULL,
  `visibility` DOUBLE NULL,
  `wind_speed` DOUBLE NULL,
  `wind_deg` DOUBLE NULL,
  `clouds` DOUBLE NULL,
  `dt` int(11) NULL,
  `sunrise` int(11)  NULL,
  `sunset` int(11)  NULL,
  KEY(`city`),
  KEY(`city`, `dt`),
  UNIQUE (`city`, `dt`)
) CHARACTER SET utf8mb4;
