module.exports = {
    database: {
        host: 'example.com',
        port: 3306,
        username: 'mysql',
        password: 'yourpassword',
        schema: 'default database',
    },
    APPID: '',
    server_port: 1234,
};
